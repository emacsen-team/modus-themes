;;; -*- no-byte-compile: t -*-
(define-package "modus-themes" "4.3.0" "set of accessible themes conforming with WCAG AAA accessibility standard" 'nil :url "https://gitlab.com/protesilaos/modus-themes")
